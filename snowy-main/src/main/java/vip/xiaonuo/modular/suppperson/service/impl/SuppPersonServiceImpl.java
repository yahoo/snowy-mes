/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppperson.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.suppperson.Result.SuppPersonResult;
import vip.xiaonuo.modular.suppperson.entity.SuppPerson;
import vip.xiaonuo.modular.suppperson.enums.SuppPersonExceptionEnum;
import vip.xiaonuo.modular.suppperson.mapper.SuppPersonMapper;
import vip.xiaonuo.modular.suppperson.param.SuppPersonParam;
import vip.xiaonuo.modular.suppperson.service.SuppPersonService;

import java.util.List;

/**
 * 供应商联系人service接口实现类
 *
 * @author zjk
 * @date 2022-08-04 13:41:10
 */
@Service
public class SuppPersonServiceImpl extends ServiceImpl<SuppPersonMapper, SuppPerson> implements SuppPersonService {

    @Override
    public PageResult<SuppPersonResult> page(SuppPersonParam suppPersonParam) {
        QueryWrapper<SuppPersonResult> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(suppPersonParam)) {

            // 根据联系人 查询
            if (ObjectUtil.isNotEmpty(suppPersonParam.getSuppPerson())) {
                queryWrapper.lambda().like(SuppPerson::getSuppPerson, suppPersonParam.getSuppPerson());
            }
            // 根据联系人电话 查询
            if (ObjectUtil.isNotEmpty(suppPersonParam.getSuppPhone())) {
                queryWrapper.lambda().like(SuppPerson::getSuppPhone, suppPersonParam.getSuppPhone());
            }
            // 根据备注 查询
            if (ObjectUtil.isNotEmpty(suppPersonParam.getRemark())) {
                queryWrapper.like("n.remark", suppPersonParam.getRemark());
            }
        }
        queryWrapper.orderByDesc("n.create_time");
        return new PageResult<>(baseMapper.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<SuppPerson> list(SuppPersonParam suppPersonParam) {
        return this.list();
    }

    @Override
    public void add(SuppPersonParam suppPersonParam) {
        checkParam(suppPersonParam,false);
        SuppPerson suppPerson = new SuppPerson();
        BeanUtil.copyProperties(suppPersonParam, suppPerson);
        this.save(suppPerson);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SuppPersonParam> suppPersonParamList) {
        suppPersonParamList.forEach(suppPersonParam -> {
            this.removeById(suppPersonParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SuppPersonParam suppPersonParam) {
        checkParam(suppPersonParam,false);
        SuppPerson suppPerson = this.querySuppPerson(suppPersonParam);
        BeanUtil.copyProperties(suppPersonParam, suppPerson);
        this.updateById(suppPerson);
    }

    @Override
    public SuppPerson detail(SuppPersonParam suppPersonParam) {
        return this.querySuppPerson(suppPersonParam);
    }

    /**
     * 获取供应商联系人
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    private SuppPerson querySuppPerson(SuppPersonParam suppPersonParam) {
        SuppPerson suppPerson = this.getById(suppPersonParam.getId());
        if (ObjectUtil.isNull(suppPerson)) {
            throw new ServiceException(SuppPersonExceptionEnum.NOT_EXIST);
        }
        return suppPerson;
    }

    @Override
    public void export(SuppPersonParam suppPersonParam) {
        List<SuppPerson> list = this.list(suppPersonParam);
        PoiUtil.exportExcelWithStream("SnowySuppPerson.xls", SuppPerson.class, list);
    }



    /**
     * 校验参数，检查是否存在相同的名称
     */
    private void checkParam(SuppPersonParam cusPersonParam, boolean isExcludeSelf) {
        Long id = cusPersonParam.getId();
        //主联系人
        String maiorCusPerson = cusPersonParam.getChiefSuppPerson();
        Long cusInforId = cusPersonParam.getSuppDataId();

        // 查询出所有的联系人
        LambdaQueryWrapper<SuppPerson> queryWrapperByPersion = new LambdaQueryWrapper<>();
        queryWrapperByPersion.eq(SuppPerson::getSuppDataId, cusInforId);
        queryWrapperByPersion.eq(SuppPerson::getChiefSuppPerson, maiorCusPerson);
        if (isExcludeSelf) {
            queryWrapperByPersion.ne(SuppPerson::getId, id);
        }
        int countByMaiorPersion = this.count(queryWrapperByPersion);

        if (countByMaiorPersion >= 1) {
            throw new ServiceException(1, "主联系人重复，请重新选择");
        }
    }

}