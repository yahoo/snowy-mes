package vip.xiaonuo.modular.workorderbill.enums;

import lombok.Getter;

@Getter
public enum WorkOrderBillEnum {
    /**
     * 销售订单
     */
    SaleOrder(1,"销售订单"),
    /**
     * 生产计划
     */
    ProPlan(2,"生产计划"),

    /**
     * 装配工单
     */
    Ass(3, "装配工单"),
    ;



    private final long code;

    private final String message;

    WorkOrderBillEnum(long code, String message) {
        this.code = code;
        this.message = message;
    }

}

