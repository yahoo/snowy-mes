package vip.xiaonuo.modular.invIn.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.cusperson.entity.CusPerson;
import vip.xiaonuo.modular.invIn.entity.InvIn;

/**
 * @author 苏辰
 */
@Data
public class invInResult extends InvIn {
    /**
     * 仓库名称
     */
    @Excel(name = "仓库名称")
    private String warHouName;
}
