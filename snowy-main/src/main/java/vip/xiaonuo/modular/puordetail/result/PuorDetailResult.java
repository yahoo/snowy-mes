package vip.xiaonuo.modular.puordetail.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.modular.puordetail.entity.PuorDetail;
@Data
public class PuorDetailResult extends PuorDetail {
    /**
     * 产品编码
     */
    private String proCode;
    /**
     * 产品名称
     */
    private String proName;
    /**
     * 采购订单编码
     */
    private String purchaseOrder;
    /**
     * 采购订单来源
     */
    private String orderSource;
    /**
     * 到货时间时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private String arrivalTime;

    /**
     * 采购来源
     */
    private String orderSourceName;


}
