/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workordertask.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.workordertask.entity.WorkOrderTask;
import vip.xiaonuo.modular.workordertask.enums.WorkOrderTaskExceptionEnum;
import vip.xiaonuo.modular.workordertask.mapper.WorkOrderTaskMapper;
import vip.xiaonuo.modular.workordertask.param.WorkOrderTaskParam;
import vip.xiaonuo.modular.workordertask.service.WorkOrderTaskService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工单任务关系表service接口实现类
 *
 * @author czw
 * @date 2022-06-07 10:17:09
 */
@Service
public class WorkOrderTaskServiceImpl extends ServiceImpl<WorkOrderTaskMapper, WorkOrderTask> implements WorkOrderTaskService {

    @Override
    public PageResult<WorkOrderTask> page(WorkOrderTaskParam workOrderTaskParam) {
        QueryWrapper<WorkOrderTask> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(workOrderTaskParam)) {

            // 根据源节点任务 查询
            if (ObjectUtil.isNotEmpty(workOrderTaskParam.getSourceTaskId())) {
                queryWrapper.lambda().eq(WorkOrderTask::getSourceTaskId, workOrderTaskParam.getSourceTaskId());
            }
            // 根据目标节点任务 查询
            if (ObjectUtil.isNotEmpty(workOrderTaskParam.getTargetTaskId())) {
                queryWrapper.lambda().eq(WorkOrderTask::getTargetTaskId, workOrderTaskParam.getTargetTaskId());
            }
            // 根据工单id 查询
            if (ObjectUtil.isNotEmpty(workOrderTaskParam.getWorkOrderId())) {
                queryWrapper.lambda().eq(WorkOrderTask::getWorkOrderId, workOrderTaskParam.getWorkOrderId());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<WorkOrderTask> list(WorkOrderTaskParam workOrderTaskParam) {
        return this.list();
    }

    @Override
    public void add(WorkOrderTaskParam workOrderTaskParam) {
        WorkOrderTask workOrderTask = new WorkOrderTask();
        BeanUtil.copyProperties(workOrderTaskParam, workOrderTask);
        this.save(workOrderTask);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<WorkOrderTaskParam> workOrderTaskParamList) {
        workOrderTaskParamList.forEach(workOrderTaskParam -> {
            this.removeById(workOrderTaskParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(WorkOrderTaskParam workOrderTaskParam) {
        WorkOrderTask workOrderTask = this.queryWorkOrderTask(workOrderTaskParam);
        BeanUtil.copyProperties(workOrderTaskParam, workOrderTask);
        this.updateById(workOrderTask);
    }

    @Override
    public WorkOrderTask detail(WorkOrderTaskParam workOrderTaskParam) {
        return this.queryWorkOrderTask(workOrderTaskParam);
    }

    /**
     * 获取工单任务关系表
     *
     * @author czw
     * @date 2022-06-07 10:17:09
     */
    private WorkOrderTask queryWorkOrderTask(WorkOrderTaskParam workOrderTaskParam) {
        WorkOrderTask workOrderTask = this.getById(workOrderTaskParam.getId());
        if (ObjectUtil.isNull(workOrderTask)) {
            throw new ServiceException(WorkOrderTaskExceptionEnum.NOT_EXIST);
        }
        return workOrderTask;
    }

    @Override
    public void export(WorkOrderTaskParam workOrderTaskParam) {
        List<WorkOrderTask> list = this.list(workOrderTaskParam);
        PoiUtil.exportExcelWithStream("SnowyWorkOrderTask.xls", WorkOrderTask.class, list);
    }

}
