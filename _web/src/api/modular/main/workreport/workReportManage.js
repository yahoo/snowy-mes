import { axios } from '@/utils/request'

/**
 * 查询报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportPage (parameter) {
  return axios({
    url: '/workReport/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 报工表列表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportList (parameter) {
  return axios({
    url: '/workReport/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportAdd (parameter) {
  return axios({
    url: '/workReport/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportEdit (parameter) {
  return axios({
    url: '/workReport/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportDelete (parameter) {
  return axios({
    url: '/workReport/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
export function workReportExport (parameter) {
  return axios({
    url: '/workReport/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
/**
 * 审批报工表
 *
 * @author 楊楊
 * @date 2022-06-30 14:08:42
 */
export function workReportApproval (parameter) {
  return axios({
    url: '/workReport/approval',
    method: 'post',
    params: parameter
  })
}
