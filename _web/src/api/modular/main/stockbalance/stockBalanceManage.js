import { axios } from '@/utils/request'

/**
 * 查询库存余额
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalancePage (parameter) {
  return axios({
    url: '/stockBalance/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库存余额列表
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalanceList (parameter) {
  return axios({
    url: '/stockBalance/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库存余额
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalanceAdd (parameter) {
  return axios({
    url: '/stockBalance/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库存余额
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalanceEdit (parameter) {
  return axios({
    url: '/stockBalance/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库存余额
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalanceDelete (parameter) {
  return axios({
    url: '/stockBalance/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出库存余额
 *
 * @author czw
 * @date 2022-07-28 16:41:37
 */
export function stockBalanceExport (parameter) {
  return axios({
    url: '/stockBalance/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
