import { axios } from '@/utils/request'

/**
 * 查询字段配置
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigPage (parameter) {
  return axios({
    url: '/fieldConfig/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 字段配置列表
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigList (parameter) {
  return axios({
    url: '/fieldConfig/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加字段配置
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigAdd (parameter) {
  return axios({
    url: '/fieldConfig/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑字段配置
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigEdit (parameter) {
  return axios({
    url: '/fieldConfig/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除字段配置
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigDelete (parameter) {
  return axios({
    url: '/fieldConfig/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出字段配置
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
export function fieldConfigExport (parameter) {
  return axios({
    url: '/fieldConfig/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
